provider "aws" {
  region = "us-east-1"
}


resource "aws_eip" "runner_ip" {
  instance = aws_instance.gitlab-runner.id
  tags = {
    Name  = "Gitlab-Runner IP"
  }
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_instance" "gitlab-runner" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.gitlab-runner.id]
  key_name = "id_rsa"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    ENV  = "RUNNER"
  }

  lifecycle {
    create_before_destroy = true
  }

}


resource "aws_security_group" "gitlab-runner" {
  name        = "Gitlab-runner Security Group for Ansible"
  description = "Security group for configuring gitlab-runner via Ansible"


  dynamic "ingress" {
    for_each = ["80", "22", "9100", "9080", "9096"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Gitlab-runner SecurityGroup for Ansible"
  }
}

output "gitlab-runner_ip" {
  description = "Elatic IP address assigned to Gitlab-runner"
  value       = aws_eip.runner_ip.public_ip
}
